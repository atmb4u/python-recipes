print """\nSimplest way to loop, over range 0 to 9 ( 10 times)
"""
for i in range(0, 10):
	print i


carly_marks = [20, 19, 14, 18, 20]
print """\nenumerate returns two values, a counter of the 
current item and the item itself.
"""
for count, mark in enumerate(carly_marks):
	print count, mark

