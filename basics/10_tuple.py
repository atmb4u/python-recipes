"""
Strings and Tuples are immutable, means, you can't edit it.
"""

alice_shopping_list = ('Groceries', 'MakeUp Set', 'MakeUp Set', 'Broom')

print "Tuple is", alice_shopping_list

print "Count of %s is %d" % (alice_shopping_list[1], alice_shopping_list.count('MakeUp Set'))
print "Count of %s is %d" % ("Useful Items", alice_shopping_list.count('Useful Items'))
print "Index of %s is %d" % (alice_shopping_list[3], alice_shopping_list.index('Broom'))