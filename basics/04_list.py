"""
Meet Mr. Doe. He's a bakery owner and got 5 items in his inventory.
He needs to keep his items in an ordered way, and able to access it easily.
"""

doe_store = [
			 "Cookie",
			 "Pastry", 
			 "Pizza", 
			 "Coffee", 
			 "Laddu"
			 ]

print "doe_store has ", doe_store
print "first item is %s" % doe_store[0]
print "3rd item is %s" % doe_store[2]

print """\nIterating over the doe_store for each item and printing it
"""
for item in doe_store:
	print item

print "\nList comprehension: Adding a little Doe's customization"
print ["Doe's %s" % item for item in doe_store]

print """\nNow, if Doe wants to add one more item to the List"""
doe_store[4] = "Mousse"
print doe_store

print """\nWhat if Doe wants to remove one item from the List"""
doe_store.remove("Coffee")
print doe_store

print """\nSort the List"""
doe_store.sort()
print doe_store

print """\nReverse the List"""
doe_store.reverse()
print doe_store

print """\nSlice the List"""
print doe_store[1:3]

print """\nThere is a lot more you can do with list; 
You can insert any type of data into a list. 
Try a mixture of str and int / list of lists."""