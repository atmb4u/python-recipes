carly_marks = [20, 19, 14, 18, 20]

while carly_marks:
	print carly_marks.pop()

print carly_marks  
print "beware: pop really pops out the value from the list, and makes it null."

print """\nYet another example with a counter"""  
counter = 5
while counter > 2:
	print counter
	counter -= 1