"""
Alice needs to add a few numbers recursively.
So she wrote a function for that.
"""

def alice_adder(num1, num2):
	return num1 + num2

total_sum = alice_adder(1001, 721)
print total_sum

recursive = alice_adder(alice_adder(100,50), alice_adder(50,75))
print recursive