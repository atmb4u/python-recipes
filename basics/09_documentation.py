"""
Python code is supposed to be very much readable, and well documented as well.
Users should be able to understand the logic simply by looking at it.

DocTests
Module showing how doctests can be included with source code
Each '>>>' line is run as if in a python shell, and counts as a test.
The next line, if not '>>>' is the expected output of the previous line.
If anything doesn't match exactly (including trailing spaces), the test fails.
"""

def multiply(a, b):
    """
    >>> multiply(4, 3)
    12
    >>> multiply('a', 3)
    'aaa'
    """
    return a * b  # or you can document as a is multiplied by b, like this

print multiply(10, 32)
"""
To run this, use 
python -m doctest -v 09_documentation.py
"""