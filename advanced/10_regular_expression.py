"""
re - regular expression - matching patterns in python

Methods
compile() - Compile the regex
match() - Determine if the RE matches at the beginning of the string.
search() - Scan through a string, looking for any location where this RE matches.
findall() - Find all substrings where the RE matches, and returns them as a list.
finditer() - Find all substrings where the RE matches, and returns them as an iterator.

Matching characters
\d - Matches any decimal digit; this is equivalent to the class [0-9].
\D - Matches any non-digit character; this is equivalent to the class [^0-9].
\s - Matches any whitespace character; this is equivalent to the class [ \t\n\r\f\v].
\S - Matches any non-whitespace character; this is equivalent to the class [^ \t\n\r\f\v].
\w - Matches any alphanumeric character; this is equivalent to the class [a-zA-Z0-9_].
\W - Matches any non-alphanumeric character; this is equivalent to the class [^a-zA-Z0-9_].
. ^ $ * + ? { } [ ] \ | ( )

"""
import re
p = re.compile('\d+')
print "p.findall('NCC 1701') : ", p.findall('NCC 1701')