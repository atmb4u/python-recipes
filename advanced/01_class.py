import datetime

class Human(object)	:
	"""
	A class for modelling humans with name and age
	"""
	name = "No Name"
	age = 0
	def year_of_birth(self):
		"""
		self is the default argument for all member functions or methods in a class.
		self can be used to access functions or values within the objects.
		"""
		timed = datetime.datetime.now() - datetime.timedelta(days=365 * self.age)
		print "%s is born in %s." %(self.name, timed.year)
		return

	def __init__(self, name="", age=0):
		if name:
			self.name = name
		self.age = age

	def husband(self, spouse):
		print "%s is married to %s." % (self.name, spouse.name)
		return

	def __gt__(self, otherclass):
		if otherclass.total_marks < self.total_marks:
			return True
		else:
			return False

bob = Human('Bob', 51)
print "%s is %d years old." % (bob.name, bob.age)
alice = Human('Alice', 49)
print "%s is %d years old." % (alice.name, alice.age)

bob.husband(alice)

bob.year_of_birth()