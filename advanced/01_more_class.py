class PrintMessage(object):
	def __init__(self, message):
		self.set_message(message)
	def print_message(self):
		print self.message
		return
	def set_message(self, message):
		try:
			self.message = message
		except:
			print "Message could not be set"

class PrintSuccessMessage(PrintMessage):
	"""docstring for PrintSuccessMessage"""
	def __init__(self, message):
		super(PrintSuccessMessage, self).__init__(message)
		
	def print_message(self):
		print "SUCCESS::",
		super(PrintSuccessMessage, self).print_message()

class PrintErrorMessage(PrintMessage):
	"""docstring for PrintSuccessMessage"""
	def __init__(self, message):
		super(PrintErrorMessage, self).__init__(message)
		
	def print_message(self):
		print "ERROR::",
		super(PrintErrorMessage, self).print_message()

		
if __name__ == '__main__':
    pm = PrintMessage("This is success")
    pm.print_message()
    pm.set_message("This is new message")
    pm.print_message()
    psm = PrintSuccessMessage("This is a success message")
    psm.print_message()
    pem = PrintErrorMessage("This is a error message")
    pem.print_message()

